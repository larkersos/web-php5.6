@echo off
@rem php7的windows7环境安装脚本( by 王奇疏 )


@rem 启动2个服务： windows modules installer、windows update
net start "TrustedInstaller"
net start "wuauserv"

@rem 安装vc14
@rem cd "D:/web_php7/soft/vc14(vs2015)"
@rem start vc_redist.2015.x86.exe

@rem 进入apache/bin目录启动2个程序
d:
cd "D:/web_php7/soft/Apache24/bin"
httpd -k install
httpd -k start
start ApacheMonitor.exe



@rem 输出提示，打开网址
echo "php7安装成功"

@rem  start http://127.0.0.1/phpinfo.php
start http://127.0.0.1/index.php
pause
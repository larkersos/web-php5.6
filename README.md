# web-php5.6
web-php5.6
使用说明:
请将web_php7压缩包解压到d:盘 , D:\web_php5.6 ，
运行D:\web_php5.6/install.bat
就可以运行了http://127.0.0.1/index.php了


软件说明：

主要用于给大家演示php的windows开发环境，
简单集成了php7 、apache2.4.这2个软件。


爱折腾的同学，可按照这篇文章操作。
http://www.cnblogs.com/wangqishu/p/5028031.html


（如果放在其他目录，请修改httpd.conf中的路径和php.ini的配置）

使用的软件具体版本是：
apache: httpd-2.4.17-win32-VC14
php7:   php-5.6.31-Win32-VC11-x86 


目前版本www下包含了 owncloud8
